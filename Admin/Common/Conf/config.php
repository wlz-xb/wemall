<?php
return array(
	//'配置项'=>'配置值'
	'DB_TYPE'   => 'mysql', // 数据库类型
	'DB_HOST'   => 'localhost', // 服务器地址
	'DB_NAME'   => 'bKnrGDXVdkvwSJfeatWj', // 数据库名
	'DB_USER'   => 'root', // 用户名
	'DB_PWD'    => 'root',  // 密码
	'DB_PORT'   => '3306', // 端口
	'DB_PREFIX' => 'wemall_', // 数据库表前缀
	'DB_CHARSET'=>'utf8',// 数据库编码默认采用utf8
	
	'URL_ROUTER_ON'   => true,
	'SHOW_PAGE_TRACE' => true,
	
	'TMPL_L_DELIM'    =>    '{{',
	'TMPL_R_DELIM'    =>    '}}',
);